import Vue from 'vue'
import vueScroll from "vuescroll";

Vue.use(vueScroll);

Vue.prototype.$vuescrollConfig = {
    bar: {
        background: 'rgb(64,67,78)'
    }
}
