import Vue from 'vue'
import vueDropZone from 'vue2-dropzone';

export default ({ app, env }) => {
    Vue.use(vueDropZone);
};

