export const ProposalStationStatusOpen : string = 'open';
export const ProposalStationStatusApproved : string = 'approved';
export const ProposalStationStatusRejected : string = 'rejected';

export const ProposalStationStatusOptions = {
    ProposalStationStatusOpen,
    ProposalStationStatusApproved,
    ProposalStationStatusRejected
}
