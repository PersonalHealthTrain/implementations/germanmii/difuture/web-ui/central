# PHT UI
This repository contains the Frontend of the UI.

## Installation
This package requires docker to be installed on the host machine.

## Configuration
The following settings need to be added to the environment file `.env` in the root directory.
```
API_URL=http://localhost:3002
RESULT_SERVICE_API_URL=http://localhost:3003
```

## Credits
If you have any questions, feel free to contact the author Peter Placzek of the project.
The project was initial developed during this bachelor thesis, and he worked after that as employee
on the project.
