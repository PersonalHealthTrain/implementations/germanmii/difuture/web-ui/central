export enum TrainResultStatus {
    DOWNLOADING = 'downloading',
    DOWNLOADED = 'downloaded',
    EXTRACTING = 'extracting',
    EXTRACTED = 'extracted',
    FINISHED = 'finished',
    FAILED = 'failed'
}
